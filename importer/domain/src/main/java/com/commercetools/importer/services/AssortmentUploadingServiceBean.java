package com.commercetools.importer.services;

import com.commercetools.importer.AggregatorServiceClient;
import com.commercetools.importer.AssortmentUploadingService;
import com.commercetools.importer.CsvParsingService;
import com.commercetools.importer.configuration.UploadingConfiguration;
import com.commercetools.importer.dto.Product;
import com.commercetools.importer.exceptions.FileTransmissionException;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.concurrent.Semaphore;

@Service
public class AssortmentUploadingServiceBean implements AssortmentUploadingService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AssortmentUploadingServiceBean.class);

    private final Semaphore uploadLimiter;
    private final CsvParsingService csvParsingService;
    private final AggregatorServiceClient aggregatorServiceClient;

    @Autowired
    public AssortmentUploadingServiceBean(UploadingConfiguration uploadingConfiguration,
                                          CsvParsingService csvParsingService,
                                          AggregatorServiceClient aggregatorServiceClient) {
        uploadLimiter = new Semaphore(uploadingConfiguration.getMaxParallelUploads());
        this.csvParsingService = csvParsingService;
        this.aggregatorServiceClient = aggregatorServiceClient;
    }

    @Override
    public boolean acceptFileForUploading(InputStream dataInputStream, boolean withHeader) {

        if (!uploadLimiter.tryAcquire()) {
            closeSilently(dataInputStream);
            return false;
        }

        try {
            Collection<Product> products = csvParsingService.parseCsv(readInputStream(dataInputStream), withHeader);
            aggregatorServiceClient.updateOnlineShopAssortment(products);
        } finally {
            closeSilently(dataInputStream);
            uploadLimiter.release();
        }

        return true;
    }

    byte[] readInputStream(InputStream dataInputStream) {
        try {
            return IOUtils.toByteArray(dataInputStream);
        } catch (IOException e) {
            throw new FileTransmissionException(e);
        }
    }

    void closeSilently(InputStream dataInputStream) {
        try {
            dataInputStream.close();
        } catch (IOException e) {
            LOGGER.warn("Could not close file upload stream cleanly.", e);
        }
    }

}
