package com.commercetools.importer.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UploadingConfiguration {

    @Value("${com.commercetools.importer.maxparalleluploads:3}")
    private int maxParallelUploads;

    public int getMaxParallelUploads() {
        return maxParallelUploads;
    }

    public void setMaxParallelUploads(int maxParallelUploads) {
        this.maxParallelUploads = maxParallelUploads;
    }

}
