package com.commercetools.importer.services;

import com.commercetools.importer.CsvParsingService;
import com.commercetools.importer.dto.Product;
import com.commercetools.importer.exceptions.CsvParsingException;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class NaiveCsvParsingServiceBean implements CsvParsingService {

    private static final int EXPECTED_CSV_RECORD_SIZE_BYTES = 100;

    private enum AssortmentCSVHeader {
        UUID,
        NAME,
        DESCRIPTION,
        PROVIDER,
        AVAILABLE,
        MEASUREMENTUNITS;
    }

    @Override
    public Collection<Product> parseCsv(byte[] csvBytes, boolean withHeader) throws CsvParsingException {
        Iterable<CSVRecord> records = csvRecordIteratorFor(csvBytes, withHeader);
        List<Product> result = new ArrayList<>(heuristicOutputProductsCount(csvBytes));

        for (CSVRecord record : records) {
            result.add(parseCsvRecord(record));
        }

        return result;
    }

    Iterable<CSVRecord> csvRecordIteratorFor(byte[] csvBytes, boolean withHeader) {
        if (withHeader) {
            return csvRecordIteratorFirstRowHeader(csvBytes);
        } else {
            return csvRecordIteratorDefaultHeader(csvBytes);
        }
    }

    Iterable<CSVRecord> csvRecordIteratorFirstRowHeader(byte[] csvBytes) {
        try {
            return CSVFormat.RFC4180
                    .withIgnoreHeaderCase(true)
                    .withFirstRecordAsHeader()
                    .parse(new InputStreamReader(new ByteArrayInputStream(csvBytes)));
        } catch (IOException e) {
            throw new CsvParsingException(e);
        }
    }

    Iterable<CSVRecord> csvRecordIteratorDefaultHeader(byte[] csvBytes) {
        try {
            return CSVFormat.RFC4180
                    .withIgnoreHeaderCase(true)
                    .withHeader(AssortmentCSVHeader.class)
                    .parse(new InputStreamReader(new ByteArrayInputStream(csvBytes)));
        } catch (IOException e) {
            throw new CsvParsingException(e);
        }
    }

    Product parseCsvRecord(CSVRecord csvRecord) {
        String uuid = csvRecord.get(AssortmentCSVHeader.UUID);
        String name = csvRecord.get(AssortmentCSVHeader.NAME);
        String description = csvRecord.get(AssortmentCSVHeader.DESCRIPTION);
        boolean available = Boolean.parseBoolean(csvRecord.get(AssortmentCSVHeader.AVAILABLE));
        String provider = csvRecord.get(AssortmentCSVHeader.PROVIDER);
        String measurementUnits = csvRecord.get(AssortmentCSVHeader.MEASUREMENTUNITS);
        return new Product(uuid, name, description, provider, available, measurementUnits);
    }

    int heuristicOutputProductsCount(byte[] csvBytes) {
        return csvBytes.length / EXPECTED_CSV_RECORD_SIZE_BYTES + 1;
    }
    
}
