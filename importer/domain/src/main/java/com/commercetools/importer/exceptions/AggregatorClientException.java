package com.commercetools.importer.exceptions;

public class AggregatorClientException extends AssortmentImporterException {

    public AggregatorClientException() {
        super();
    }

    public AggregatorClientException(String message) {
        super(message);
    }

    public AggregatorClientException(String message, Throwable cause) {
        super(message, cause);
    }

    public AggregatorClientException(Throwable cause) {
        super(cause);
    }

}
