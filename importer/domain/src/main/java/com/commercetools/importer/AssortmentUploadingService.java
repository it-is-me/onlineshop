package com.commercetools.importer;

import com.commercetools.importer.exceptions.AssortmentImporterException;

import java.io.InputStream;

public interface AssortmentUploadingService {

    boolean acceptFileForUploading(InputStream dataInputStream, boolean withHeader) throws AssortmentImporterException;

}
