package com.commercetools.importer.dto;

public class Product {

    private final String uuid;
    private final String name;
    private final String description;
    private final String provider;
    private final boolean available;
    private final String measurementUnits;

    public Product(String uuid,
                   String name,
                   String description,
                   String provider,
                   boolean available,
                   String measurementUnits) {
        this.uuid = uuid;
        this.name = name;
        this.description = description;
        this.provider = provider;
        this.available = available;
        this.measurementUnits = measurementUnits;
    }

    public String getUuid() {
        return uuid;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getProvider() {
        return provider;
    }

    public boolean isAvailable() {
        return available;
    }

    public String getMeasurementUnits() {
        return measurementUnits;
    }

}
