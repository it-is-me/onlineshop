package com.commercetools.importer.exceptions;

public class FileTransmissionException extends AssortmentImporterException {

    public FileTransmissionException(String message) {
        super(message);
    }

    public FileTransmissionException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public FileTransmissionException(Throwable throwable) {
        super(throwable);
    }

}
