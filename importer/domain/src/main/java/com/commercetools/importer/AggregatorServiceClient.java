package com.commercetools.importer;

import com.commercetools.importer.dto.Product;
import com.commercetools.importer.exceptions.AggregatorClientException;

import java.util.Collection;

public interface AggregatorServiceClient {

    void updateOnlineShopAssortment(Collection<Product> products) throws AggregatorClientException;

}
