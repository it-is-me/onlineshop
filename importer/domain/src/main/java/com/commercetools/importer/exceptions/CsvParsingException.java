package com.commercetools.importer.exceptions;

public class CsvParsingException extends AssortmentImporterException {

    public CsvParsingException(Exception rootCause) {
        super(rootCause);
    }

    public CsvParsingException(String message, Exception rootCause) {
        super(message, rootCause);
    }

}
