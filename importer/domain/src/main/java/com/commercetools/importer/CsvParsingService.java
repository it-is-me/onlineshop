package com.commercetools.importer;

import com.commercetools.importer.dto.Product;
import com.commercetools.importer.exceptions.CsvParsingException;

import java.util.Collection;

public interface CsvParsingService {

    /**
     * Parses csv file as a collection of online shop products.
     *
     * @param csvBytes csv bytes (will be treated as UTF-8 without BOM, RFC 4180)
     * @param withHeader if csv document includes header
     * @return parsed collection of online shop products
     * @throws CsvParsingException if something went wrong
     */
    Collection<Product> parseCsv(byte[] csvBytes, boolean withHeader) throws CsvParsingException;

}
