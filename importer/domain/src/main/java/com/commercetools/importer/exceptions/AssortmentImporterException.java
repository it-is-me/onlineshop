package com.commercetools.importer.exceptions;

public class AssortmentImporterException extends RuntimeException {

    public AssortmentImporterException() {
        super();
    }

    public AssortmentImporterException(String message) {
        super(message);
    }

    public AssortmentImporterException(String message, Throwable cause) {
        super(message, cause);
    }

    public AssortmentImporterException(Throwable cause) {
        super(cause);
    }

}
