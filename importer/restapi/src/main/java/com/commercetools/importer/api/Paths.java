package com.commercetools.importer.api;

public interface Paths {

    String V1 = "/v1";
    String PRODUCTS = "/products";
    String V1_PRODUCTS = V1 + PRODUCTS;

}
