package com.commercetools.importer.api.controllers;

import com.commercetools.importer.api.Paths;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

public interface ShopAssortmentController {

    /**
     *  Allows to update or create the online shop's products
     *
     * @param file csv-file with products to be updated or created in UTF-8
     * @param containsHeader true/false, designates if the csv file to be uploaded
     *                       contains a header row
     * @return response with:
     * {@link org.springframework.http.HttpStatus#ACCEPTED} upload task has been scheduled
     * {@link org.springframework.http.HttpStatus#TOO_MANY_REQUESTS} if the service is overloaded
     * {@link org.springframework.http.HttpStatus#PAYLOAD_TOO_LARGE} in case if the payload is too large
     *      or zip-bomb had been thrown
     * {@link org.springframework.http.HttpStatus#BAD_REQUEST} if the request is invalid
     *
     */
    @RequestMapping(path = Paths.V1_PRODUCTS, method = RequestMethod.POST,
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    ResponseEntity<Void> upload(@RequestParam("file") MultipartFile file,
                                @RequestParam(name = "containsHeader", required = false, defaultValue = "true")
                                        String containsHeader);

}
