package com.commercetools.importer.services;

import com.commercetools.aggregator.api.dto.Product;
import org.springframework.stereotype.Component;

@Component
public class ProductMapper {

    Product mapToExternalDto(com.commercetools.importer.dto.Product domainDto) {
        return new Product(domainDto.getUuid(),
                domainDto.getName(),
                domainDto.getDescription(),
                domainDto.getProvider(),
                domainDto.isAvailable(),
                domainDto.getMeasurementUnits());
    }
}
