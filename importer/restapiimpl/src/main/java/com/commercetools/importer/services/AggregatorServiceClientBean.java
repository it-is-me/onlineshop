package com.commercetools.importer.services;

import com.commercetools.importer.AggregatorServiceClient;
import com.commercetools.importer.configuration.AggregatorClientConfig;
import com.commercetools.importer.dto.Product;
import com.commercetools.importer.exceptions.AggregatorClientException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AggregatorServiceClientBean implements AggregatorServiceClient {

    private final ProductMapper productMapper;
    private final AggregatorClientConfig clientConfig;

    @Autowired
    public AggregatorServiceClientBean(ProductMapper productMapper,
                                       AggregatorClientConfig clientConfig) {
        this.productMapper = productMapper;
        this.clientConfig = clientConfig;
    }

    @Override
    public void updateOnlineShopAssortment(Collection<Product> products) throws AggregatorClientException {
        List<com.commercetools.aggregator.api.dto.Product> mappedDtos =
                products.stream().map(productMapper::mapToExternalDto).collect(Collectors.toList());

        updateProducts(mappedDtos);
    }

    void updateProducts(List<com.commercetools.aggregator.api.dto.Product> products) {
        RestTemplate restTemplate = new RestTemplate();
        RequestEntity<List<com.commercetools.aggregator.api.dto.Product>> rq =
                createRequest(products);

        ResponseEntity<Void> response = restTemplate.exchange(rq, Void.class);

        assertUploadWasSuccessful(response);
    }

    void assertUploadWasSuccessful(ResponseEntity<Void> response) throws AggregatorClientException {
        if (HttpStatus.OK != response.getStatusCode()) {
            throw new AggregatorClientException(response.getStatusCode().value() + " : "
                    + response.getStatusCode().getReasonPhrase());
        }
    }

    RequestEntity<List<com.commercetools.aggregator.api.dto.Product>> createRequest(
            List<com.commercetools.aggregator.api.dto.Product> products) {
        return RequestEntity.put(buildUri()).body(products);
    }

    URI buildUri() {
        return URI.create("http://" + clientConfig.getHost()
                + ":" + clientConfig.getPort()
                + clientConfig.getPath());
    }

}
