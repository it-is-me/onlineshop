package com.commercetools.importer.configuration;

import com.commercetools.aggregator.api.Paths;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AggregatorClientConfig {

    @Value("${com.commercetools.importer.aggregatorclient.host:localhost}")
    private String host;

    @Value("${com.commercetools.importer.aggregatorclient.port:30400}")
    private int port;

    @Value("${com.commercetools.importer.aggregatorclient.path:/v1/products}")
    private String path;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getPath() {
        return (path != null && !path.isEmpty()) ? path : Paths.V1_PRODUCTS;
    }

    public void setPath(String path) {
        this.path = path;
    }

}
