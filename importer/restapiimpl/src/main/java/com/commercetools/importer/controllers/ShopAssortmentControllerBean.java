package com.commercetools.importer.controllers;

import com.commercetools.importer.AssortmentUploadingService;
import com.commercetools.importer.api.Paths;
import com.commercetools.importer.api.controllers.ShopAssortmentController;
import com.commercetools.importer.exceptions.FileTransmissionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;

@Controller
public class ShopAssortmentControllerBean implements ShopAssortmentController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ShopAssortmentControllerBean.class);

    private final AssortmentUploadingService uploadingService;

    @Autowired
    public ShopAssortmentControllerBean(AssortmentUploadingService uploadingService) {
        this.uploadingService = uploadingService;
    }

    @RequestMapping(path = Paths.V1_PRODUCTS, method = RequestMethod.POST,
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<Void> upload(@RequestParam("file") MultipartFile file,
                                       @RequestParam(name = "containsHeader", required = false, defaultValue = "true")
                                                   String containsHeader) {
        if (!uploadingService.acceptFileForUploading(inputStreamSilently(file), Boolean.parseBoolean(containsHeader))) {
            return ResponseEntity.status(HttpStatus.TOO_MANY_REQUESTS).build();
        } else {
            return ResponseEntity.accepted().build();
        }
    }

    InputStream inputStreamSilently(MultipartFile multipartFile) {
        try {
            return multipartFile.getInputStream();
        } catch (IOException e) {
            final String message = "Cannot open input stream for CSV file uploading!";
            LOGGER.error(message, e);
            throw new FileTransmissionException(message, e);
        }
    }

}
