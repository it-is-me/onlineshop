package com.commercetools.aggregator;

import com.commercetools.aggregator.api.Paths;
import com.commercetools.aggregator.api.dto.Product;
import com.commercetools.aggregator.api.dto.ProductStaistics;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;
import static org.springframework.restdocs.webtestclient.WebTestClientRestDocumentation.document;
import static org.springframework.restdocs.webtestclient.WebTestClientRestDocumentation.documentationConfiguration;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = {
                AggregatorApplication.class
        },
        webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT
)
@AutoConfigureWebTestClient
@AutoConfigureRestDocs(outputDir = "target/generated-snippets")
public class AggregatorApplicationIT {

        @Rule
        public final JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation();

        private WebTestClient client;

        @Before
        public void setUp() {
                client = WebTestClient
                        .bindToServer()
                        .baseUrl("http://localhost:8080")
                        .filter(documentationConfiguration(this.restDocumentation))
                        .build();
        }

        @Test
        public void incrementalUpload() {
                // WHEN
                ProductStaistics noUploads = getStatistics();

                // THEN
                assertThat(noUploads).isNotNull();
                assertThat(noUploads.getProductsWereCreated()).isEqualTo(0);
                assertThat(noUploads.getProductsWereUpdated()).isEqualTo(0);
                assertThat(noUploads.getTimestamp())
                        .isCloseTo(ZonedDateTime.now(), within(5, ChronoUnit.SECONDS));

                // GIVEN
                final String uuid = UUID.randomUUID().toString();
                final String name = "a name";
                final String description = "a description";
                final String provider = "a provider";
                final boolean available = true;
                final String measurementUnits = "some units";
                final Product productToCreate =
                        new Product(uuid, name, description, provider, available, measurementUnits);

                // AND WHEN
                putProducts(Collections.singletonList(productToCreate));
                ProductStaistics oneCreated = getStatistics();

                // THEN
                assertThat(oneCreated).isNotNull();
                assertThat(oneCreated.getProductsWereCreated()).isEqualTo(1);
                assertThat(oneCreated.getProductsWereUpdated()).isEqualTo(0);
                assertThat(oneCreated.getTimestamp())
                        .isCloseTo(ZonedDateTime.now(), within(5, ChronoUnit.SECONDS));

                // GIVEN
                final String updatedDescription = "better description";
                final Product productToUpdate =
                        new Product(uuid, name, updatedDescription, provider, available, measurementUnits);

                // WHEN
                putProducts(Collections.singletonList(productToUpdate));
                ProductStaistics oneUpdated = getStatistics();

                // THEN
                assertThat(oneUpdated).isNotNull();
                assertThat(oneUpdated.getProductsWereCreated()).isEqualTo(1);
                assertThat(oneUpdated.getProductsWereUpdated()).isEqualTo(1);
                assertThat(oneUpdated.getTimestamp())
                        .isCloseTo(ZonedDateTime.now(), within(5, ChronoUnit.SECONDS));

                // WHEN
                List<Product> products = getProducts();

                assertThat(products)
                        .isNotNull()
                        .hasSize(1);

                Product result = products.get(0);

                // THEN
                assertThat(result.getUuid()).isEqualTo(uuid);
                assertThat(result.getName()).isEqualTo(name);
                assertThat(result.getDescription()).isEqualTo(updatedDescription);
                assertThat(result.getProvider()).isEqualTo(provider);
                assertThat(result.isAvailable()).isEqualTo(available);
                assertThat(result.getMeasurementUnits()).isEqualTo(measurementUnits);
        }


        private ProductStaistics getStatistics() {
                return client.get().uri(Paths.V1_STATISTICS)
                        .exchange()
                        .expectStatus().isOk()
                        .expectBody(ProductStaistics.class)
                        .consumeWith(document("get"))
                        .returnResult().getResponseBody();
        }

        private List<Product> getProducts() {
                return client.get().uri(Paths.V1_PRODUCTS)
                        .exchange()
                        .expectStatus().isOk()
                        .expectBodyList(Product.class)
                        .consumeWith(document("get"))
                        .returnResult().getResponseBody();
        }

        private void putProducts(List<Product> products) {
                client.put().uri(Paths.V1_PRODUCTS)
                        .syncBody(products)
                        .exchange()
                        .expectStatus().isOk()
                        .expectBody(Void.class)
                        .consumeWith(document("put"))
                        .returnResult().getResponseBody();
        }

}
