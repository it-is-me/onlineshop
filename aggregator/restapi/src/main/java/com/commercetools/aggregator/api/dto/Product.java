package com.commercetools.aggregator.api.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Product {

    private final String uuid;
    private final String name;
    private final String description;
    private final String provider;
    private final boolean available;
    private final String measurementUnits;

    @JsonCreator
    public Product(@JsonProperty("uuid") String uuid,
                   @JsonProperty("name") String name,
                   @JsonProperty("description") String description,
                   @JsonProperty("provider") String provider,
                   @JsonProperty("available") boolean available,
                   @JsonProperty("measurementUnits") String measurementUnits) {
        this.uuid = uuid;
        this.name = name;
        this.description = description;
        this.provider = provider;
        this.available = available;
        this.measurementUnits = measurementUnits;
    }

    public String getUuid() {
        return uuid;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getProvider() {
        return provider;
    }

    public boolean isAvailable() {
        return available;
    }

    public String getMeasurementUnits() {
        return measurementUnits;
    }

}
