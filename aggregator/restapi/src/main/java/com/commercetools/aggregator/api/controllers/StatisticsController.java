package com.commercetools.aggregator.api.controllers;

import com.commercetools.aggregator.api.Paths;
import com.commercetools.aggregator.api.dto.ProductStaistics;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

public interface StatisticsController {

    /**
     *  Provides daily shop product statistics
     *
     * @return statistics object, containing number of updated and created products
     * {@link org.springframework.http.HttpStatus#OK} in case of success
     * {@link org.springframework.http.HttpStatus#INTERNAL_SERVER_ERROR} if something went wrong
     */
    @RequestMapping(path = Paths.V1_STATISTICS, method = RequestMethod.GET)
    ResponseEntity<ProductStaistics> showDailyProductStatistics();

}
