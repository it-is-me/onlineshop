package com.commercetools.aggregator.api.controllers;

import com.commercetools.aggregator.api.Paths;
import com.commercetools.aggregator.api.dto.Product;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

public interface ProductsController {

    /**
     *  Provides all the products in the online shop
     *
     * @return list of all products of the shop with:
     * {@link org.springframework.http.HttpStatus#OK} in case of success
     * {@link org.springframework.http.HttpStatus#INTERNAL_SERVER_ERROR} if something went wrong
     */
    @RequestMapping(path = Paths.V1_PRODUCTS, method = RequestMethod.GET)
    ResponseEntity<List<Product>> getProducts();

    /**
     *  Massively uploads products to the online shop
     *
     * @param products products to be updated or created
     * @return response with status:
     * {@link org.springframework.http.HttpStatus#OK} in case of success
     * {@link org.springframework.http.HttpStatus#INTERNAL_SERVER_ERROR} if something went wrong
     */
    @RequestMapping(path = Paths.V1_PRODUCTS, method = RequestMethod.PUT)
    ResponseEntity<Void> bulkUpload(@RequestBody List<Product> products);

}
