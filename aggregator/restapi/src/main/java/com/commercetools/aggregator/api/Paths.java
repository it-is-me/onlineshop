package com.commercetools.aggregator.api;

public interface Paths {

    String V1 = "/v1";
    String PRODUCTS = "/products";
    String V1_PRODUCTS = V1 + PRODUCTS;
    String STATISTICS = "/statistics";
    String V1_STATISTICS = V1 + STATISTICS;

}
