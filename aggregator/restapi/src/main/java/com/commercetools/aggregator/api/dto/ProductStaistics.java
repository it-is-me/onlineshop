package com.commercetools.aggregator.api.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.ZonedDateTime;

public class ProductStaistics {

    private final ZonedDateTime timestamp;
    private final int productsWereCreated;
    private final int productsWereUpdated;

    @JsonCreator
    public ProductStaistics(
            @JsonProperty("timestamp") ZonedDateTime timestamp,
            @JsonProperty("productsWereCreated") int productsWereCreated,
            @JsonProperty("productsWereUpdated") int productsWereUpdated) {
        this.timestamp = timestamp;
        this.productsWereCreated = productsWereCreated;
        this.productsWereUpdated = productsWereUpdated;
    }

    public ZonedDateTime getTimestamp() {
        return timestamp;
    }

    public int getProductsWereCreated() {
        return productsWereCreated;
    }

    public int getProductsWereUpdated() {
        return productsWereUpdated;
    }

}
