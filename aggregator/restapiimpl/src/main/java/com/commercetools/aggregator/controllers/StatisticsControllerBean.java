package com.commercetools.aggregator.controllers;

import com.commercetools.aggregator.DailyStatistics;
import com.commercetools.aggregator.ProductStatisticsProvider;
import com.commercetools.aggregator.api.Paths;
import com.commercetools.aggregator.api.controllers.StatisticsController;
import com.commercetools.aggregator.api.dto.ProductStaistics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class StatisticsControllerBean implements StatisticsController {

    private final ProductStatisticsProvider productStatisticsProvider;

    @Autowired
    public StatisticsControllerBean(ProductStatisticsProvider productStatisticsProvider) {
        this.productStatisticsProvider = productStatisticsProvider;
    }

    @Override
    @RequestMapping(path = Paths.V1_STATISTICS, method = RequestMethod.GET)
    public ResponseEntity<ProductStaistics> showDailyProductStatistics() {
        DailyStatistics actualStatistics = productStatisticsProvider.getActualStatistics();

        return ResponseEntity.ok(new ProductStaistics(
                actualStatistics.getActuality(),
                actualStatistics.getCreatedProducts(),
                actualStatistics.getUpdatedProducts()
        ));
    }

}
