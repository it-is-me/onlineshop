package com.commercetools.aggregator.controllers;

import com.commercetools.aggregator.api.dto.Product;
import org.springframework.stereotype.Component;

@Component
public class ProductMapper {

    Product mapToDto(com.commercetools.aggregator.entities.Product entity) {
        return new Product(
                entity.getUuid(),
                entity.getName(),
                entity.getDescription(),
                entity.getProvider(),
                entity.getAvailable(),
                entity.getMeasurementUnits()
        );
    }

    com.commercetools.aggregator.entities.Product fillIntoEntity(Product dto) {
        com.commercetools.aggregator.entities.Product entity =
                new com.commercetools.aggregator.entities.Product();

        entity.setUuid(dto.getUuid());
        entity.setName(dto.getName());
        entity.setDescription(dto.getDescription());
        entity.setAvailable(dto.isAvailable());
        entity.setMeasurementUnits(dto.getMeasurementUnits());
        entity.setProvider(dto.getProvider());
        return entity;
    }

}
