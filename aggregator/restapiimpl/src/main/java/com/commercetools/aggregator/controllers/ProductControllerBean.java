package com.commercetools.aggregator.controllers;

import com.commercetools.aggregator.ProductService;
import com.commercetools.aggregator.api.Paths;
import com.commercetools.aggregator.api.controllers.ProductsController;
import com.commercetools.aggregator.api.dto.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class ProductControllerBean implements ProductsController {

    private final ProductService productService;
    private final ProductMapper productMapper;

    @Autowired
    public ProductControllerBean(ProductService productService, ProductMapper productMapper) {
        this.productService = productService;
        this.productMapper = productMapper;
    }

    @Override
    @RequestMapping(path = Paths.V1_PRODUCTS, method = RequestMethod.GET)
    public ResponseEntity<List<Product>> getProducts() {
        return ResponseEntity.ok(productService
                .getAll()
                .stream()
                .map(productMapper::mapToDto)
                .collect(Collectors.toList()));
    }

    @Override
    @RequestMapping(path = Paths.V1_PRODUCTS, method = RequestMethod.PUT)
    public ResponseEntity<Void> bulkUpload(@RequestBody List<Product> products) {
        products
                .stream()
                .map(productMapper::fillIntoEntity)
                .forEach(productService::createOrUpdate);

        return ResponseEntity.status(HttpStatus.OK).build();
    }

}
