package com.commercetools.aggregator;

public interface ProductStatisticsConsumer {

    void productCreated();

    void productUpdated();

}
