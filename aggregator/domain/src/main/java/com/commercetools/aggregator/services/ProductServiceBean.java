package com.commercetools.aggregator.services;

import com.commercetools.aggregator.ProductService;
import com.commercetools.aggregator.ProductStatisticsConsumer;
import com.commercetools.aggregator.entities.Product;
import com.commercetools.aggregator.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceBean implements ProductService {

    private static final int SELECTION_BUFFER_SIZE = 10000;

    private final ProductRepository productRepository;
    private final ProductStatisticsConsumer statisticsConsumer;

    @Autowired
    public ProductServiceBean(ProductRepository productRepository, ProductStatisticsConsumer statisticsConsumer) {
        this.productRepository = productRepository;
        this.statisticsConsumer = statisticsConsumer;
    }

    @Override
    @Transactional
    public void createOrUpdate(Product product) {
        Optional<Product> foundByUuid = productRepository.findByUuid(product.getUuid());
        if (foundByUuid.isPresent()) {
            updateProductIfNeeded(foundByUuid.get(), product);
        } else {
            productRepository.save(product);
            statisticsConsumer.productCreated();
        }

    }

    @Override
    public Collection<Product> getAll() {
        // This "get all" method is a part of the task.
        // generally it is not a good idea to implement "getAll" REST
        // interface (the selection size can be too big and cause OOM error,
        // pagination functionality is slow and not consistent).
        // In many cases it is better to use file-based approaches
        // or in the worst case - pagination, if we really need them all online.
        List<Product> result = new ArrayList<>(SELECTION_BUFFER_SIZE);
        productRepository.findAll().forEach(result::add);
        return result;
    }

    private void updateProductIfNeeded(Product existing, Product candidate) {
        candidate.setId(existing.getId());
        if (candidate.equals(existing)) {
            return;
        }
        productRepository.save(candidate);
        statisticsConsumer.productUpdated();
    }

}
