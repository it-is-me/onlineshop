package com.commercetools.aggregator;

public interface ProductStatisticsProvider {

    DailyStatistics getActualStatistics();

}
