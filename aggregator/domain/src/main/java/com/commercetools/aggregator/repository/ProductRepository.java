package com.commercetools.aggregator.repository;

import com.commercetools.aggregator.entities.Product;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ProductRepository extends CrudRepository<Product, Long> {

    Optional<Product> findByUuid(String uuid);

}
