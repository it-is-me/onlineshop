package com.commercetools.aggregator;

import com.commercetools.aggregator.entities.Product;

import java.util.Collection;

public interface ProductService {

    void createOrUpdate(Product product);

    Collection<Product> getAll();

}
