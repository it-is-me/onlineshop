package com.commercetools.aggregator;

import java.time.ZonedDateTime;

public class DailyStatistics {

    private final int createdProducts;
    private final int updatedProducts;
    private final ZonedDateTime actuality;

    public DailyStatistics(int createdProducts,
                           int updatedProducts,
                           ZonedDateTime actuality) {
        this.createdProducts = createdProducts;
        this.updatedProducts = updatedProducts;
        this.actuality = actuality;
    }

    public int getCreatedProducts() {
        return createdProducts;
    }

    public int getUpdatedProducts() {
        return updatedProducts;
    }

    public ZonedDateTime getActuality() {
        return actuality;
    }

}
