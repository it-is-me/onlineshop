package com.commercetools.aggregator.services;

import com.commercetools.aggregator.DailyStatistics;
import com.commercetools.aggregator.ProductStatisticsConsumer;
import com.commercetools.aggregator.ProductStatisticsProvider;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Implementation of the statistics service does not keep its state in a persistent
 * storage and will be lost after the service restart.
 *
 * As an alternative, PrometheusMetrics can be used in the implementation
 *
 * It is designed to be efficient and lightweight, to be used for monitoring purposes.
 */
@Service
public class InMemoryProductStatisticsServiceBean
        implements ProductStatisticsConsumer, ProductStatisticsProvider {

    /*
        For the sake of efficiency and atomicity counter is implemented
        using single long, keeping entire state
     */
    // 28 least significant bits
    private static final long MASK_CREATED = 0x0000_0000_0FFF_FFFFL;
    // bits from 29 to 58
    private static final long MASK_UPDATED = 0x00FF_FFFF_F000_0000L;
    // 8 most significant bits (including the sign bit), shown for clarity
    //private static final long MASK_DAY_OF_MONTH = 0xFF00_0000_0000_0000L;
    private static final int BIT_SHIFT_UPDATED = 28;
    private static final int BIT_SHIFT_DAY_OF_MONTH = 58;
    private static final long ONE_UPDATE = 1 << BIT_SHIFT_UPDATED;

    private final AtomicLong counter;

    public InMemoryProductStatisticsServiceBean() {
        this.counter = new AtomicLong();
    }

    @Override
    public void productCreated() {
        resetCounterIfNeeded();
        counter.incrementAndGet();
    }

    @Override
    public void productUpdated() {
        resetCounterIfNeeded();
        counter.addAndGet(ONE_UPDATE);
    }

    @Override
    public DailyStatistics getActualStatistics() {
        while (true) {
            resetCounterIfNeeded();
            long encodedData = counter.get();
            ZonedDateTime now = getNow();
            if (fetchEncodedDayOfMonth(encodedData) != now.getDayOfMonth()) {
                continue;
            }
            return new DailyStatistics(fetchCreated(encodedData),
                    fetchUpdated(encodedData),
                    now);
        }
    }

    ZonedDateTime getNow() {
        return ZonedDateTime.now();
    }

    private void resetCounterIfNeeded() {
        while (true) {
            long currentValue = counter.get();
            int dayOfMonth = getDayOfMonth();
            if (fetchEncodedDayOfMonth(currentValue) == dayOfMonth) {
                return;
            }
            if (counter.compareAndSet(currentValue, encodeForNewDay(dayOfMonth))) {
                return;
            }
        }
    }

    private int fetchEncodedDayOfMonth(long encodedValue) {
        return (int) (encodedValue >>> BIT_SHIFT_DAY_OF_MONTH);
    }

    private int getDayOfMonth() {
        return getNow().getDayOfMonth();
    }

    private long encodeForNewDay(int dayOfMonth) {
        return ((long) dayOfMonth) << BIT_SHIFT_DAY_OF_MONTH;
    }

    private int fetchCreated(long encodedValue) {
        resetCounterIfNeeded();
        return (int) (encodedValue & MASK_CREATED);
    }

    private int fetchUpdated(long encodedValue) {
        resetCounterIfNeeded();
        return (int) ((encodedValue & MASK_UPDATED) >>> BIT_SHIFT_UPDATED);
    }

}
