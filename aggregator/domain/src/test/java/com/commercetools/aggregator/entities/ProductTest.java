package com.commercetools.aggregator.entities;

import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ProductTest {

    private static final String UUID = java.util.UUID.randomUUID().toString();
    private static final Long ID = (long)(Math.random() * Long.MAX_VALUE);
    private static final String DESCRIPTION = "the very best phone!";
    private static final String NAME = "some name";
    private static final String MEASUREMENT_UNITS = "some unit";
    private static final String PROVIDER = "some provider";
    private static final Boolean AVAILABLE = Boolean.TRUE;

    private Product instance;

    @Before
    public void setUp() {
        instance = new Product();
        instance.setId(ID);
        instance.setUuid(UUID);
        instance.setDescription(DESCRIPTION);
        instance.setName(NAME);
        instance.setProvider(PROVIDER);
        instance.setMeasurementUnits(MEASUREMENT_UNITS);
        instance.setAvailable(AVAILABLE);
    }

    @Test
    public void getId() {
        assertThat(instance.getId()).isEqualTo(ID);
    }

    @Test
    public void getUuid() {
        assertThat(instance.getUuid()).isEqualTo(UUID);
    }

    @Test
    public void getName() {
        assertThat(instance.getName()).isEqualTo(NAME);
    }

    @Test
    public void getDescription() {
        assertThat(instance.getDescription()).isEqualTo(DESCRIPTION);
    }

    @Test
    public void getProvider() {
        assertThat(instance.getProvider()).isEqualTo(PROVIDER);
    }

    @Test
    public void getAvailable() {
        assertThat(instance.getAvailable()).isEqualTo(AVAILABLE);
    }

    @Test
    public void getMeasurementUnits() {
        assertThat(instance.getMeasurementUnits()).isEqualTo(MEASUREMENT_UNITS);
    }

    @Test
    public void equalsWorksCorrectly() {
        EqualsVerifier.forClass(Product.class);
    }

}