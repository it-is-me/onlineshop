package com.commercetools.aggregator;

import org.junit.Before;
import org.junit.Test;

import java.time.ZonedDateTime;

import static org.assertj.core.api.Assertions.assertThat;

public class DailyStatisticsTest {

    private static final int CREATED_PRODUCTS = 777;
    private static final int UPDATED_PRODUCTS = 333;
    private static final ZonedDateTime ACTUALITY = ZonedDateTime.now();

    private DailyStatistics instance;

    @Before
    public void setUp() {
        instance = new DailyStatistics(CREATED_PRODUCTS, UPDATED_PRODUCTS, ACTUALITY);
    }


    @Test
    public void getCreatedProducts() {
        assertThat(instance.getCreatedProducts()).isEqualTo(CREATED_PRODUCTS);
    }

    @Test
    public void getUpdatedProducts() {
        assertThat(instance.getUpdatedProducts()).isEqualTo(UPDATED_PRODUCTS);
    }

    @Test
    public void getActuality() {
        assertThat(instance.getActuality()).isEqualTo(ACTUALITY);
    }

}