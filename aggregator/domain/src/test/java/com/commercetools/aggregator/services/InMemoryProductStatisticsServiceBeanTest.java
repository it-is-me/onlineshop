package com.commercetools.aggregator.services;

import com.commercetools.aggregator.DailyStatistics;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.ZonedDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class InMemoryProductStatisticsServiceBeanTest {

    private static final ZonedDateTime TEST_TIME = ZonedDateTime.now();

    private InMemoryProductStatisticsServiceBean instance;

    @Before
    public void setUp() {
        instance = Mockito.mock(InMemoryProductStatisticsServiceBean.class,
                Mockito.withSettings()
                        .useConstructor()
                        .defaultAnswer(Mockito.CALLS_REAL_METHODS));
    }

    @Test
    public void productCreated() {
        //GIVEN
        given(instance.getNow()).willReturn(TEST_TIME);
        final DailyStatistics beforeCall = instance.getActualStatistics();

        //WHEN
        instance.productCreated();

        //THEN
        final DailyStatistics afterCall = instance.getActualStatistics();

        assertThat(beforeCall).isNotNull();
        assertThat(afterCall).isNotNull();
        assertThat(afterCall.getCreatedProducts()).isEqualTo(beforeCall.getCreatedProducts() + 1);
        assertThat(afterCall.getUpdatedProducts()).isEqualTo(beforeCall.getUpdatedProducts());
        assertThat(afterCall.getActuality()).isEqualTo(beforeCall.getActuality());
    }

    @Test
    public void productUpdated() {
        //GIVEN
        given(instance.getNow()).willReturn(TEST_TIME);
        final DailyStatistics beforeCall = instance.getActualStatistics();

        //WHEN
        instance.productUpdated();

        //THEN
        final DailyStatistics afterCall = instance.getActualStatistics();

        assertThat(beforeCall).isNotNull();
        assertThat(afterCall).isNotNull();
        assertThat(afterCall.getCreatedProducts()).isEqualTo(beforeCall.getCreatedProducts());
        assertThat(afterCall.getUpdatedProducts()).isEqualTo(beforeCall.getUpdatedProducts() + 1);
        assertThat(afterCall.getActuality()).isEqualTo(beforeCall.getActuality());
    }

    @Test
    public void counterResetOnNewDay() {
        //GIVEN
        final ZonedDateTime today = ZonedDateTime.now().withHour(0);
        final ZonedDateTime tomorrow = today.plusDays(1);
        given(instance.getNow()).willReturn(today);

        //WHEN
        instance.productCreated();
        instance.productUpdated();

        //THEN
        given(instance.getNow()).willReturn(tomorrow);

        //AND WHEN
        final DailyStatistics onNewDay = instance.getActualStatistics();

        assertThat(onNewDay).isNotNull();
        assertThat(onNewDay.getCreatedProducts()).isEqualTo(0);
        assertThat(onNewDay.getUpdatedProducts()).isEqualTo(0);
        assertThat(onNewDay.getActuality()).isEqualTo(tomorrow);
    }

}