package com.commercetools.aggregator.services;

import com.commercetools.aggregator.ProductStatisticsConsumer;
import com.commercetools.aggregator.entities.Product;
import com.commercetools.aggregator.repository.ProductRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceBeanTest {

    @Mock
    private Product savingProduct;
    @Mock
    private Product productFromRepository;
    @Mock
    private Product anotherProductFromRepository;
    @Mock
    private ProductRepository productRepository;
    @Mock
    private ProductStatisticsConsumer statisticsConsumer;

    @InjectMocks
    private ProductServiceBean instance;

    @Test
    public void getAll() {
        // GIVEN
        given(productRepository.findAll())
                .willReturn(Arrays.asList(productFromRepository, anotherProductFromRepository));

        // WHEN
        Collection<Product> result = instance.getAll();

        // THEN
        assertThat(result)
                .isNotNull()
                .isNotEmpty()
                .containsExactlyInAnyOrder(productFromRepository, anotherProductFromRepository);

        verifyZeroInteractions(statisticsConsumer, productFromRepository, anotherProductFromRepository);
        verify(productRepository).findAll();
        verifyNoMoreInteractions(productRepository);
    }

    @Test
    public void createProduct() {
        // GIVEN
        final String uuid = UUID.randomUUID().toString();
        given(savingProduct.getUuid()).willReturn(uuid);
        given(productRepository.findByUuid(uuid)).willReturn(Optional.empty());

        // WHEN
        instance.createOrUpdate(savingProduct);

        // THEN
        verify(productRepository).findByUuid(uuid);
        verify(productRepository).save(savingProduct);
        verify(savingProduct).getUuid();
        verify(statisticsConsumer).productCreated();
        verifyNoMoreInteractions(productRepository, savingProduct, statisticsConsumer);
    }

    @Test
    public void updateProduct() {
        // GIVEN
        final String uuid = UUID.randomUUID().toString();
        final long id = (long) (Math.random() * Long.MAX_VALUE);
        given(savingProduct.getUuid()).willReturn(uuid);
        given(productRepository.findByUuid(uuid)).willReturn(Optional.of(productFromRepository));
        given(productFromRepository.getId()).willReturn(id);
        // WHEN
        instance.createOrUpdate(savingProduct);

        // THEN
        verify(productFromRepository).getId();
        verify(productRepository).findByUuid(uuid);
        verify(productRepository).save(savingProduct);
        verify(savingProduct).getUuid();
        verify(savingProduct).setId(id);
        verify(statisticsConsumer).productUpdated();
        verifyNoMoreInteractions(productRepository, savingProduct, statisticsConsumer, productFromRepository);
    }

    @Test
    public void updateProductSkippedTheSameData() {
        // GIVEN
        final String uuid = UUID.randomUUID().toString();
        final long id = (long) (Math.random() * Long.MAX_VALUE);
        final Product savingProduct = new Product();
        final Product productFromRepository = new Product();
        savingProduct.setUuid(uuid);
        productFromRepository.setUuid(uuid);
        productFromRepository.setId(id);

        given(productRepository.findByUuid(uuid)).willReturn(Optional.of(productFromRepository));

        // WHEN
        instance.createOrUpdate(savingProduct);

        // THEN
        verify(productRepository).findByUuid(uuid);
        verifyNoMoreInteractions(productRepository);
        verifyZeroInteractions(statisticsConsumer);
    }

}