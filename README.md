# onlineShop

Project was made to show my coding skills. Although it is not ready for production (strictly speaking), everything works and (hopefully) 
shows my knowledge of industrial approaches of REST-services development.

How to make it better:

- Split the project to two separate microservices completely and reduce number of sub-modules in the maven project (each of them will be simpler than the current one, containing both)
- Implement Liquibase and DB Schema constraints (currently UUID can be not unique, there is no proper protection!)
- Make code cleaner. Some methods and classes could have been simpler. 
- Write more automated integration tests using dockerized resources and maven-docker-run plugin (e.g. run and shutdown the Aggregator docker image automatically during the Imporeter integration tests)
- Write unit tests, improve code coverage up to 100% using Mockito, JUnit, AssertJ and etc.
- Implement request validation
- Implement upload file size limitation and protection from gunzip-bombs
- Fix few rare counter-related bugs (all are very rare, but still can occur in the current implementation)
-- if the Aggregator is not used for 1+ month and is used again at the same day of the month, statistics will not be reset (the same day of another month)
-- if the time is changed, statistics may be inaccurate
- Document all the endpoints, create automated documentation using AsciiDoc plugin and previously created integration tests
- Enable SpringBoot Actuator/Prometheus endpoints and use Spring/Meter functionality (probably instead- or in-addition to the current implementation)
- Distributed DB/ NoSql (?). But then no ACID-transactions and strict consistency, CAP-theorem and Amazon approach.
- Create circuit breakers, protect the endpoints using Hystrix and lightweight fallback methods
- Implement security and authorisation, use HTTPS, certificates (Java cryptography extension). Integrate our microservices with corporate directory service lookup provider if needed or OAuth/OAuth2.
- If we want to design an Internet shop (something interacting with UI), probably, it would make sense to think about HYPERMEDIA API, HateOAS.
- Load tests with JMeter/Maven